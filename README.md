# housemate firmware
ESP8266 firmware for WiFi switches.

Developed for ESP-12F.
Also tested on NodeMCU v1.0 (ESP-12E).

Version 0.1

## Build with

### Arduino IDE

##### Core libraries

* [PubSubClient](https://github.com/knolleary/pubsubclient) - MQTT
* [WiFiUDP](https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/WiFiUdp.h) - UDP
* [ESP8266WiFi](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi) - WiFi client
* [EEPROM](https://github.com/esp8266/Arduino/tree/master/libraries/EEPROM/examples) - Persistence
* [HttpUpdate](http://esp8266.github.io/Arduino/versions/2.0.0/doc/ota_updates/ota_updates.html#http-server) - OTA update

## Useful links

- https://arduino-esp8266.readthedocs.io/en/latest/index.html
- https://pubsubclient.knolleary.net/api.html
- https://docs.tuya.com/en/mcu/mcu-protocol.html

## Authors
 Nikos Bagias - email: bagias@entranet.gr