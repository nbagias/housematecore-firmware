#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <WiFiUDP.h>
#include <PubSubClient.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

static const char softApPwd[] PROGMEM = "h0us3mat3";
static const char mqttUser[] PROGMEM = "device";
static const char mqttPassword[] PROGMEM = "housemate18";
static const char mqttTopicPrefixOut[] PROGMEM = "esp/out/";
static const char mqttTopicPrefixIn[] PROGMEM = "esp/in/";
static const char mqttStatus[] PROGMEM = "#MH01#01#"; // The last part is the firmware version (0.1)
static const char udpSuccessRes[] PROGMEM = "#MH00#";
static const char udpFailRes[] PROGMEM = "#MH99#";
// The buffer size of the serial is abusively used
// as the upper limit of the EEPROM, too.
const int bufferSize = 128;
const int udpPort = 48120;
const int statusUpdateInterval = 60000;
const char delimChar = '#'; 

char messageContainer[7] = {delimChar, '1', 'H', '0', '0', delimChar, '\0'}; 

WiFiUDP udpServer;
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

IPAddress housemateIP;
int bytesRead = 0;
byte serialBuffer[bufferSize];
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];
unsigned long previousMillis = 0;

// Firmware update variables.
bool isUpdatePending = false;
char updateUrl[96];

void setup() {
  Serial.setDebugOutput(false);
  Serial.setRxBufferSize(bufferSize);
  Serial.begin(9600, SERIAL_8N1);
  EEPROM.begin(bufferSize);
  delay(100);
  if (isInitialized()) {
    sendMcuWifiState(2);
    connectToWifi();
  } else {
    sendMcuWifiState(1);
    startWifiAP();
  }
  mqttClient.setCallback(callback);
  // Either way start the UDP server.
  udpServer.begin(udpPort);
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) {
    // MQTT
    if (mqttClient.connected()) {
      // Check MCU.  
      while (Serial.available() > 0) {
        serialBuffer[bytesRead] = Serial.read();
        // If this is the last expected byte.
        if (bytesRead == 10) {
          // Check tuya header.
          if (serialBuffer[0] == 0x55 && serialBuffer[1] == 0xAA) {
            // Check type command.
            if (serialBuffer[3] == 7 && serialBuffer[5] == 5) {
              // Ignore the third gang, we will not have such type.
              if (serialBuffer[6] == 0x01 || serialBuffer[6] == 0x02) {
                if (serialBuffer[6] == 0x01) {
                  messageContainer[1] = '1';
                } else if (serialBuffer[6] == 0x02) {
                  messageContainer[1] = '2';
                }
                if (serialBuffer[10] == 0x00) {
                   messageContainer[4] = '0';
                } else {
                   messageContainer[4] = '1';
                }
                char topic[30];
                strcpy_P(topic, mqttTopicPrefixOut);
                delay(100);
                strcat(topic, WiFi.macAddress().c_str());
                mqttClient.publish(topic, messageContainer);
              }
            }
          }
          // Cleanup.
          bytesRead = 0;
          memset(serialBuffer, 0, bufferSize);
          // Dump checksum. Maybe do the right thing in the future.
          Serial.read();
          break; 
        }
        // Check for long press signals.
        if (bytesRead == 7 && serialBuffer[3] == 4) {          
          clearEeprom();
          ESP.restart();
          delay(10);
        }
        bytesRead++;
      }
      mqttClient.loop();
      // Periodically send status messages.
      unsigned long currentMillis = millis();
      if ((unsigned long)(currentMillis - previousMillis) >= statusUpdateInterval) {
        char topic[30];
        strcpy_P(topic, mqttTopicPrefixOut);
        delay(100);
        strcat(topic, WiFi.macAddress().c_str());
        char statusMsg[15];
        strcpy_P(statusMsg, mqttStatus);
        mqttClient.publish(topic, statusMsg);
        previousMillis = currentMillis;
      }
    } else {
      // Make it a clean cut.
      mqttClient.disconnect();
      delay(500);
    }
  } else {
    if (WiFi.getMode() != WIFI_AP) {
      connectToWifi();
    }
  }
  // Otherwise, check for UDP messages.
  int packetSize = udpServer.parsePacket();
  if (packetSize) {
    int len = udpServer.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    if (len > 5) {
      packetBuffer[len] = 0;
      // Parse and take action.
      handleMessage(packetBuffer);
    }
    memset(packetBuffer, 0, UDP_TX_PACKET_MAX_SIZE);
  }
  // Handle firmware updates.
  if (isUpdatePending) {
    updateFirmware();
  }
  yield();
}

/**
 * MQTT callback. 
 * 
 * Handles messages from the subscribed topics.
 */
void callback(char* topic, byte* payload, unsigned int length) {
  if (length > 5) {
    char cpayload[96];
    strncpy(cpayload, (char*)payload, length);
    cpayload[length] = 0;
    handleMessage(cpayload);
  }
}

/**
 * Helper function for debugging issues.
 * // TODO Remove it when done.
 */
//void sendUdp() {
//  IPAddress ip(255, 255, 255, 255);
//  udpServer.beginPacket(ip, 43000);
//  udpServer.write("yeap", strlen("yeap"));
//  udpServer.endPacket();
//}

/**
 * Matches the incoming message with the protocol and routes action appropriately.
 */
void handleMessage(char* message) {
  if (message[0] == delimChar && message[5] == delimChar) {
    // Only parse messages that come from housemate (no evil exists).
    if (message[1] == 'H') {
      switch (message[2]) {
        case '0':
        case '1':
          if (message[4] == '0') {
            sendMcuCommand(0, 0);
          } else {
            sendMcuCommand(0, 1);
          }
          break;
        case '2':
           if (message[4] == '0') {
            sendMcuCommand(1, 0);
          } else {
            sendMcuCommand(1, 1);
          }
          break;
        case 'M':
          if (message[4] == '0') {
            // Ignore if already connected.
            if (mqttClient.connected()) break;
            
            housemateIP = udpServer.remoteIP();
            connectToBroker();
          } else if (message[4] == '1') {
            if (saveCredentials(message)) {
              sendUdpResponse(true);
              delay(10);
              connectToWifi();
            } else {
              sendUdpResponse(false);
            }
          } else if (message[4] == '2') {
            for (int i = 6; i < strlen(message); i++) {
              // Ignore last character.
              if (message[i] == delimChar) break;
              updateUrl[i-6] = message[i];
            }
            isUpdatePending = true;
          }
          break;
        case 'A':
          sendMcuCommand(0, message[4]);
          delay(10);
          sendMcuCommand(1, message[4]);
          break;
        default:
          break;
      }
    }
  }
}

/**
 * Sets up the MQTT client and connects to the broker. 
 * 
 * This assumes that the broker's IP is known.
 * Which requires that an advertisement was previously received.
 */
void connectToBroker() {
  mqttClient.setServer(housemateIP, 1883);
  char inTopic[35];
  strcpy_P(inTopic, mqttTopicPrefixIn);
  delay(100);
  strcat(inTopic, WiFi.macAddress().c_str());
  char outTopic[35];
  strcpy_P(outTopic, mqttTopicPrefixOut);
  delay(100);
  strcat(outTopic, WiFi.macAddress().c_str());
  char user[15];
  strcpy_P(user, mqttUser);
  char pwd[20];
  strcpy_P(pwd, mqttPassword);
  delay(100);
  if (mqttClient.connect(WiFi.macAddress().c_str(), user, pwd, outTopic, 1, true, "#MH98#")) {
    char statusMsg[15];
    strcpy_P(statusMsg, mqttStatus);
    mqttClient.publish(outTopic, statusMsg);
    mqttClient.subscribe(inTopic);
  }
}

/**
 * Reads the WiFi credentials from EEPROM and connects to the provided network.
 * 
 * Restarts if connection keeps failing for more than 10 secs.
 */
void connectToWifi() {
  // Read SSID.
  char storedSsid[32];
  for (int i = 0; i < 32; ++i) {
    storedSsid[i] = EEPROM.read(i + 1);
  }
  if (strlen(storedSsid) > 1) {
    // Read Password.
    char storedPwd[64];
    for (int i = 0; i < 64; ++i) {
      storedPwd[i] = EEPROM.read(i + 33);
    }
    // Connect.
    WiFi.mode(WIFI_STA);
    WiFi.begin(storedSsid, storedPwd);
    int k = 0;
    // Wait for connection to succeed.
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        if (k > 10) {
          delay(1000);
          ESP.restart();
        }
        k++;
    }
    sendMcuWifiState(3);
  }
}

/**
 * Parses the provided parameter, extracts the WiFi credentials
 * and saves them to EEPROM.
 * 
 * Restores EEPROM if error occured.
 * 
 * @return true if saved, false if something went wrong.
 */
bool saveCredentials(char* message) {
  char* token = strtok(message, "#");
  if (token != NULL) {
    // This is the ssid (hopefully).
    token = strtok (NULL, "#");
    if (token != NULL) {
      if (strlen(token) > 32) {
        return false;
      }
      EEPROM.write(0, delimChar);
      for (int i = 0; i < strlen(token); ++i) {
        EEPROM.write(1 + i, token[i]);
      }
      token = strtok (NULL, "#");
      if (token != NULL) {
        if (strlen(token) > 64 || strlen(token) < 8) {
          clearEeprom();
          return false;
        }
        for (int i = 0; i < strlen(token); ++i) {
          EEPROM.write(33 + i, token[i]);
        }
      } else {
        // Clean SSID.
        clearEeprom();
        return false;
      }
      EEPROM.commit();
      delay(10);
      return true;
    }
  }
  return false;
}

/**
 * Enters pairing mode, which means setting up an AP with its
 */
void startWifiAP() {
  char ssid[18];
  char prefix[3] = "H_";
  strcpy(ssid, prefix);
  strcat(ssid, WiFi.macAddress().c_str());
  char pwd[13];
  strncpy_P(pwd, softApPwd, strlen_P(softApPwd));
  delay(100);
  bool result = WiFi.softAP(ssid, pwd);
  if (!result) {
    delay(5000);
    ESP.restart();
  }
}

/**
 * Blocking function for updating firmware.
 * 
 * The url is provided from the received message and currently is an S3 link.
 * 
 * Works for http links only.
 * 
 * Testing for https needs some certificate info:
 *  SHA1 Fingerprint - "C5 10 19 73 E5 97 A1 38 B4 BF CC 26 AD 60 F7 96 EF 18 0A 3A"
 */
void updateFirmware() {
  if (strlen(updateUrl) > 0) {
    t_httpUpdate_return ret = ESPhttpUpdate.update(updateUrl);
    char topic[30];
    strcpy_P(topic, mqttTopicPrefixOut);
    delay(100);
    strcat(topic, WiFi.macAddress().c_str());
    switch (ret) {
      case HTTP_UPDATE_FAILED:
        mqttClient.publish(topic, ESPhttpUpdate.getLastErrorString().c_str());
        break;
      case HTTP_UPDATE_NO_UPDATES:
        mqttClient.publish(topic, "#MH10#");
        break;
      case HTTP_UPDATE_OK:
        // If successful there will be an automatic restart. But it looks more complete this way.
        break;
    }
    isUpdatePending = false;
  }
}

void sendUdpResponse(bool success) {
  char response[10];
  if (success) {
    strcpy(response, udpSuccessRes);
  } else {
    strcpy(response, udpFailRes);
  }
  delay(100);
  udpServer.beginPacket(udpServer.remoteIP(), udpServer.remotePort());
  udpServer.write(response, strlen(response));
  udpServer.endPacket();
}

/**
 * Reads the first byte from the EEPROM. 
 * 
 * Any character other than the expected is regarded as an uninitialized state.
 * 
 * @return bool - true if initialized, false otherwise.
 */
bool isInitialized() {
  char firstChar = EEPROM.read(0);
  // If EEPROM does not start with the expected character, return false.
  if (firstChar != delimChar) {
    // Clear to remove junk if any.
    clearEeprom();
    return false;
  }
  return true;
}

/**
 * Clears the EEPROM. 
 * 
 * Not absolutely necessary but it can help with string termination etc.
 */
void clearEeprom() {
  for (int i = 0; i < bufferSize; i++) {
    EEPROM.write(i, 0);
  }
  EEPROM.commit();
}

/**
 * Issues ON/OFF commands through serial.
 */
void sendMcuCommand(byte btnNum, byte state) {
  Serial.write(0x55); // Tuya header 55AA
  Serial.write(0xAA);
  Serial.write(0x00); // version 00
  Serial.write(0x06); // Tuya command 06 - send order
  Serial.write(0x00);
  Serial.write(0x05); // following data length 0x05
  Serial.write((btnNum + 1)); // relay number 1,2,3
  Serial.write(0x01);
  Serial.write(0x00);
  Serial.write(0x01);
  Serial.write(state);
  Serial.write((13 + btnNum + state)); // checksum:sum of all bytes in packet mod 256
  Serial.flush();
}

/**
 * Changes the WiFi work state for the MCU.
 */
void sendMcuWifiState(byte state) {
  Serial.write(0x55);
  Serial.write(0xAA);
  Serial.write(0x00);
  Serial.write(0x03); 
  Serial.write(0x00);
  Serial.write(0x01); 
  Serial.write(state);
  Serial.write(3 + state);
  Serial.flush();
}

